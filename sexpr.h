#ifndef SEXPR_H
#define SEXPR_H

#include <memory>
#include <functional>
#include <map>

class Sexpr;
class List;
class Number;
class Symbol;
class Context;

using SexprPtr = std::shared_ptr<Sexpr>;
using ListPtr = std::shared_ptr<List>;
using NumPtr = std::shared_ptr<Number>;
using SymPtr = std::shared_ptr<Symbol>;
using Func = std::function<SexprPtr(SexprPtr)>;

class Sexpr {
    // covariant return types
    virtual SexprPtr base_clone() const = 0;
public:
    virtual ~Sexpr();
    virtual SexprPtr eval(Context &) = 0;
    virtual void print(std::ostream &os) const = 0;
    virtual void define(ListPtr, Context &) = 0;

    virtual bool is_list() const;
};

std::ostream & operator<<(std::ostream & os, Sexpr &e);

class Context {
    Context *parent_ = nullptr;
    std::map<std::string, Func> symbols;
public:
    Context();
    void parent(Context *);
    Func lookup(std::string &);
    void insert(std::string &, SexprPtr);
};

#endif // SEXPR_H
