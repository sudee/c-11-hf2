#include <iostream>
#include <memory>
#include <exception>
#include "sexpr.h"
#include "list.h"
#include "atom.h"
#include "debug.h"

Sexpr::~Sexpr() { }

bool Sexpr::is_list() const {
    return false;
}

std::ostream & operator<<(std::ostream &os, Sexpr &e) {
    e.print(os);
    return os;
}


Context::Context() {
    symbols = {
        {"+", [] (SexprPtr exp) -> SexprPtr {
            ListPtr l = std::dynamic_pointer_cast<List>(exp);
            auto it = ++l->begin();
            NumPtr a = std::dynamic_pointer_cast<Number>(*it++);
            NumPtr b = std::dynamic_pointer_cast<Number>(*it++);
            return std::make_shared<Number>(a->val + b->val);
        }},

        {"-", [] (SexprPtr exp) -> SexprPtr {
            ListPtr l = std::dynamic_pointer_cast<List>(exp);
            auto it = ++l->begin();
            NumPtr a = std::dynamic_pointer_cast<Number>(*it++);
            NumPtr b = std::dynamic_pointer_cast<Number>(*it++);
            return std::make_shared<Number>(a->val - b->val);
        }},

        {"*", [] (SexprPtr exp) -> SexprPtr {
            ListPtr l = std::dynamic_pointer_cast<List>(exp);
            auto it = ++l->begin();
            NumPtr a = std::dynamic_pointer_cast<Number>(*it++);
            NumPtr b = std::dynamic_pointer_cast<Number>(*it++);
            return std::make_shared<Number>(a->val * b->val);
        }},

        {"=", [] (SexprPtr exp) -> SexprPtr {
            ListPtr l = std::dynamic_pointer_cast<List>(exp);
            auto it = ++l->begin();
            NumPtr a = std::dynamic_pointer_cast<Number>(*it++);
            NumPtr b = std::dynamic_pointer_cast<Number>(*it++);
            return std::make_shared<Number>(a->val == b->val ? 1.0 : 0.0);
        }}
    };
}

void Context::parent(Context *p) {
    this->parent_ = p;
}

Func Context::lookup(std::string &s) {
    debug([=] { std::cout << "looking up symbol: " << s << std::endl; });
    if (symbols.count(s) == 0) {
        if (parent_) {
            return parent_->lookup(s);
        } else {
            throw std::runtime_error("undefined symbol");
        }
    }
    return symbols.at(s);
}

void Context::insert(std::string &s, SexprPtr expr) {
    symbols[s] = [expr] (SexprPtr) -> SexprPtr { return expr; };
}
