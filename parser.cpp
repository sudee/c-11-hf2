#include <cctype>
#include <sstream>
#include "parser.h"
#include "atom.h"
#include "list.h"
#include "debug.h"

Parser::Parser(std::istream &is) : is(is) { }

Parser::ParserState Parser::get_state() {
    int c = is.peek();
    if (c == '(')
        return LIST_START;
    else if (c == ')')
        return LIST_END;
    else if (isspace(c))
        return WHITESPACE;
    else if (c == ';')
        return COMMENT;
    else if (isgraph(c))
        return ATOM;
    else
        return INVALID;
}

bool Parser::atomend() {
    int c = is.peek();
    return is.eof() || isspace(c) || c == '(' || c == ')' || c == ';';
}

void Parser::skip_comment() {
    while (is.peek() != '\n')
        is.get();
}

void Parser::skip_whitespace() {
    while (isspace(is.peek()))
        is.get();
}

SexprPtr Parser::read_atom() {
    std::stringstream word;
    // type of the atom
    bool digit = isdigit(is.peek());

    while (!atomend()) {
        word << static_cast<char>(is.get());
    }

    if (digit) {
        return std::make_shared<Number>(std::stod(word.str()));
    } else {
        return std::make_shared<Symbol>(word.str());
    }
}

SexprPtr Parser::read_list() {
    ListPtr listp = std::make_shared<List>();
    SexprPtr expr;

    bool inside = true;
    // discard '('
    is.get();

    ParserState ps = get_state();
    while(is && inside) {
        if (ps == LIST_START) {
            expr = read_list();
            listp->push(expr);
        } else if (ps == LIST_END) {
            // discard ')'
            is.get();
            inside = false;
        } else if (ps == ATOM) {
            expr = read_atom();
            listp->push(expr);
        } else if (ps == WHITESPACE) {
            skip_whitespace();
        } else if (ps == COMMENT) {
            skip_comment();
        } else {
            debug([&] { std::cout << "list default (" << is.peek() << ")" << std::endl; });
        }
        ps = get_state();
    }
    return listp;
}

SexprPtr Parser::read() {
    SexprPtr expr = nullptr;
    bool success = false;
    // read one S-expression or until eof
    while (is && !success) {
        switch (get_state()) {
            case LIST_START:
                expr = read_list();
                success = true;
                break;

            case COMMENT:
                skip_comment();
                break;

            case WHITESPACE:
                skip_whitespace();
                break;

            case ATOM:
                expr = read_atom();
                success = true;
                break;

            default:
                debug([&] { std::cout << (is ? "read default" : "EOF") << std::endl; });
                break;
        }
    }
    if (expr == nullptr && is) {
        debug([&] { std::cout << "nullptr" << std::endl; });
    }
    return expr;
}
