#include <iostream>
#include <sstream>
#include "list.h"
#include "atom.h"
#include "debug.h"

List::List() { }

List::~List() {
    debug([=] { std::cout << "list dtor" << std::endl; });
}


bool List::is_list() const {
    return true;
}

SexprPtr List::eval(Context &c) {
    ListPtr l = this->clone();
    auto it = l->begin();
    SexprPtr head = *it++;
    SymPtr shead = std::dynamic_pointer_cast<Symbol>(head);

    // special case for "define"
    if (shead->val.compare("define") == 0) {
        // virtual define (symbol / list)
        (*it)->define(l, c);
        return std::make_shared<List>();
    // special case for "if"
    } else if (shead->val.compare("if") == 0) {
        auto vals = ++l->begin();
        SexprPtr cond_tmp = *vals++;
        NumPtr cond = std::dynamic_pointer_cast<Number>(cond_tmp->eval(c));
        SexprPtr trueVal = *vals++;
        SexprPtr falseVal = *vals++;
        if (cond->val == 1.0) {
            return trueVal->eval(c);
        } else {
            return falseVal->eval(c);
        }
    }

    // evaluate params
    for (it = ++(l->values.begin()); it != l->values.end(); ++it) {
        *it = (*it)->eval(c);
    }

    // evaluate function
    SexprPtr result = c.lookup(shead->val)(l);
    if (result->is_list()) {
        ListPtr l = std::dynamic_pointer_cast<List>(result);
        return eval_func(c, l);
    } else {
        return result;
    }
}

SexprPtr List::eval_func(Context &c, ListPtr func) {
    Context local;
    local.parent(&c);
    auto it = func->begin();

    ListPtr args = this->clone();
    args->values.pop_front();

    ListPtr tmp = std::dynamic_pointer_cast<List>(*it++);
    ListPtr params = tmp->clone();
    params->values.pop_front();

    ListPtr body = std::dynamic_pointer_cast<List>(*it++);

    if (args->values.size() != params->values.size()) {
        throw std::runtime_error("invalid arguments");
    }
    auto arg = args->begin();
    for (auto param = params->begin(); param != params->end(); ++param, ++arg) {
        SymPtr s = std::dynamic_pointer_cast<Symbol>(*param);
        *arg = (*arg)->eval(c);
        local.insert(s->val, *arg);
    }
    return body->eval(local);
}

void List::print(std::ostream &os) const {
    os << '(';
    std::stringstream out;
    for (auto it = values.begin(); it != values.end(); ++it) {
        (*it)->print(out);
        out << ' ';
    }
    auto outs = out.str();
    os << outs.substr(0, outs.size() - 1);
    os << ')';
}

void List::define(ListPtr expr, Context &c) {
    ListPtr func = expr->clone();
    ListPtr decl = this->clone();
    SymPtr name = std::dynamic_pointer_cast<Symbol>(*(decl->begin()));

    func->values.pop_front();

    // store the function body for later evaluation
    c.insert(name->val, std::dynamic_pointer_cast<Sexpr>(func));
}


SexprPtr List::base_clone() const {
    return std::make_shared<List>(*this);
}

ListPtr List::clone() const {
    return std::dynamic_pointer_cast<List>(base_clone());
}

void List::push(SexprPtr p) {
    values.push_back(p);
}

Iter List::begin() {
    return this->values.begin();
}

Iter List::end() {
    return this->values.end();
}
