#include <iostream>
#include <exception>
#include "sexpr.h"
#include "parser.h"
#include "debug.h"

bool DEBUG;

bool args_has(char c, int argc, char **argv) {
    for (int i = 1; i < argc; ++i) {
        if (argv[i][0] == c) return true;
    }
    return false;
}


int main(int argc, char **argv) {
    DEBUG = args_has('D', argc, argv);
    bool eval = !args_has('P', argc, argv);

    Parser p(std::cin);
    SexprPtr se;
    Context global;
    while ( (se = p.read()) ) {
        try {
            debug([=] { std::cout << *se << std::endl; });
            if (eval) {
                SexprPtr result = se->eval(global);
                std::cout << *result << std::endl;
            }
        } catch (std::runtime_error &e) {
            std::cout << e.what() << std::endl;
        }
    }
}
