extern bool DEBUG;

template <typename Func>
void debug(Func f) {
    if(DEBUG) f();
}
