#include <string>
#include <iostream>
#include <exception>
#include "atom.h"
#include "list.h"
#include "debug.h"

void Atom::define(ListPtr, Context &) {
    throw std::runtime_error("invalid argument");
}

Symbol::Symbol(std::string v) : val(v) { }

Symbol::~Symbol() {
    debug([=] { std::cout << "symbol dtor" << std::endl; });
}


SexprPtr Symbol::eval(Context &c) {
    SexprPtr result = c.lookup(this->val)(nullptr);
    return result;
}

void Symbol::print(std::ostream &os) const {
    os << val;
}

void Symbol::define(ListPtr expr, Context &c) {
    ListPtr l = expr->clone();
    auto it = ++(l->begin());
    SymPtr name = std::dynamic_pointer_cast<Symbol>(*it++);
    // eval defined body
    c.insert(name->val, std::dynamic_pointer_cast<Sexpr>((*it)->eval(c)));
}


SexprPtr Symbol::base_clone() const {
    return std::make_shared<Symbol>(*this);
}

SymPtr Symbol::clone() const {
    return std::dynamic_pointer_cast<Symbol>(base_clone());
}


Number::Number(double v) : val(v) { }

Number::~Number() {
    debug([=] { std::cout << "number dtor" << std::endl; });
}

SexprPtr Number::eval(Context &c) {
    return this->clone();
}

void Number::print(std::ostream &os) const {
    os << val;
}

SexprPtr Number::base_clone() const {
    return std::make_shared<Number>(*this);
}

NumPtr Number::clone() const {
    return std::dynamic_pointer_cast<Number>(base_clone());
}
