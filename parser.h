#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include "sexpr.h"

class Parser {
    enum ParserState { LIST_START, LIST_END, WHITESPACE, COMMENT, ATOM, INVALID };

    // input
    std::istream &is;
    // debug
    int nest_id = 0;

    ParserState get_state();
    bool atomend();

    void skip_comment();
    void skip_whitespace();

    SexprPtr read_atom();
    SexprPtr read_list();
public:
    Parser(std::istream &);
    SexprPtr read();
};

#endif // PARSER_H
