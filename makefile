BINARY = main
OBJECTS = sexpr.o parser.o atom.o list.o main.o
HEADERS = sexpr.h parser.h atom.h list.h

CC = mingw32-gcc
CFLAGS = -std=c11 -Wall -pedantic -g
LDFLAGS = -g

CXX = mingw32-g++
CXXFLAGS = -std=c++11 -Wall -pedantic -g

.PHONY: all clean

all: $(BINARY)

clean:
	del $(BINARY) $(OBJECTS)

$(BINARY): $(OBJECTS)
	$(CXX) $(LDFLAGS) $^ -o $@

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

%.o: %.cpp $(HEADERS)
	$(CXX) $(CXXFLAGS) -c $< -o $@
