#ifndef ATOM_H
#define ATOM_H

#include "sexpr.h"

class Atom : public Sexpr {
    virtual void define(ListPtr, Context &);
};


class Symbol : public Atom {
    SexprPtr base_clone() const override;
public:
    std::string val;
    Symbol(std::string v);
    ~Symbol();
    SexprPtr eval(Context &) override;
    void print(std::ostream &os) const override;
    void define(ListPtr, Context &) override;

    SymPtr clone() const;
};


class Number : public Atom {
    SexprPtr base_clone() const override;
public:
    double val;
    Number(double v);
    ~Number();
    SexprPtr eval(Context &) override;
    void print(std::ostream &os) const override;

    NumPtr clone() const;
};

#endif // ATOM_H
