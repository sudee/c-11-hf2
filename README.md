# README #

A simple Scheme interpreter for my C++11 homework assignment.

### Implemented functions ###

* `+`, `-`, `*`, `=`
* `define`, `if`

### Implemented types ###

* List
* Atom
    * Number
    * Symbol