#ifndef LIST_H
#define LIST_H

#include <list>
#include "sexpr.h"

using Iter = std::list<SexprPtr>::iterator;

class List : public Sexpr {
    std::list<SexprPtr> values;

    SexprPtr base_clone() const override;
public:
    List();
    ~List();
    SexprPtr eval(Context &);
    void print(std::ostream &) const;
    void define(ListPtr, Context &) override;
    SexprPtr eval_func(Context &, ListPtr);

    ListPtr clone() const;
    void push(SexprPtr);
    Iter begin();
    Iter end();
    bool is_list() const override;
};

#endif // LIST_H
